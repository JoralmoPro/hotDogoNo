import { SplashScreen } from '@ionic-native/splash-screen';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
// imports necesarios

@IonicPage({
  //Lazy loading
  name: "inicio"
})
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
})
export class InicioPage {
  //Variables utilizadas en la aplicación

  //Apikey de google cloud vision
  googleCloudVisionAPIKey = "TUAPIKEY";
  //Para obtener las respuestas de google cloud vision
  labels: any[] = [];
  //Para dar vista previa a la imgen
  imagen: any = null;
  //Respuesta de google cloud vison
  resultado: any = null;
  //variable de control de si es o no es hotdog
  es: boolean = false;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public splashScreen: SplashScreen, public http: Http, public loader: LoadingController, private camera: Camera, public toast: ToastController) {
      //Objetos necesarios, necesarios tambien agregarlos en el app.module.ts
  }

  ionViewDidLoad() {
    //Para ocultar el splash de ionic
    this.splashScreen.hide();
  }


  //Funcion para hacer la petición a google cloud vision, estructura necesaria para la petición segun la documentación
  getLabels(base64) {
    const body = {
      "requests": [
        {
          "image": {
            "content": base64
          },
          "features": [
            {
              "type": "LABEL_DETECTION"
            }
          ]
        }
      ]
    }
    //Retornar la respuesta
    return this.http.post(`https://vision.googleapis.com/v1/images:annotate?key=${this.googleCloudVisionAPIKey}`, body)
  }

  //Funcion para abrir la camara y procesar la imagen
  tomarFoto() {
    //Crear loader
    let loader = this.loader.create({
      content: 'Ejecutando analisis...'
    });
    //Mostrar loader
    loader.present();
    //Opciones para abrir la camara
    const opciones: CameraOptions = {
      //Calidad de la imagen
      quality: 100,
      //Alto de la imagen
      targetHeight: 500,
      //Ancho de la imagen
      targetWidth: 500,
      //Tip de respuesta (base64 en este caso)
      destinationType: this.camera.DestinationType.DATA_URL,
      //Tipo png
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      //Abrir desde la camara (se puede tambien desde la galeria)
      sourceType: this.camera.PictureSourceType.CAMERA
    }
    //Abirmos la camara pasando las opciones antes estipuladas
    this.camera.getPicture(opciones).then((img) => {
      this.labels = [];
      this.es = false;
      //Hacemos la petición a google cloud vision
      this.getLabels(img).subscribe((resultados) => {
        //Hacemos la variable imagen igual a la imagen obtenida por la camara para mostrar la vista previa
        this.imagen = img;
        //Obtenemos los resultados que nos da google
        this.resultado = resultados.json().responses;
        //Recorremos las etiquetas de la respuesta con map()
        this.resultado[0].labelAnnotations.map(obj => {
          //Guardamos las etiquetas en la variable labels
          this.labels.push(obj.description);
          //Si algunas de las etiquetas es "hot dog" entonces es un hot dog
          if (obj.description == "hot dog") this.es = true;
        });
        //Quitamos el loader
        loader.dismiss();
      }, err => {
        //Por si acaso ocurre un error
        loader.dismiss();
        this.mostrarToast(err.message, 5000);
      });
    }, err => {
      //Por si acaso ocurre un error
      loader.dismiss();
      this.mostrarToast(err.message, 5000);
    });
  }

  //Funcion para mostrar mensaje de error recibe mensaje de error y la duración de el mensaje
  mostrarToast(mensaje: string, duracion: number) {
    this.toast.create({
      message: mensaje,
      duration: duracion
    }).present();
  }

}
